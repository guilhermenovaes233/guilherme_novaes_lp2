  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
    <div class="site-wrap" id="home-section">

      <div class="site-mobile-menu site-navbar-target">
        <div class="site-mobile-menu-header">
          <div class="site-mobile-menu-close mt-3">
            <span class="icon-close2 js-menu-toggle"></span>
          </div>
        </div>
        <div class="site-mobile-menu-body"></div>
      </div>

      <header class="site-navbar site-navbar-target" role="banner">

        <div class="container mb-3">
          <div class="d-flex align-items-center">
            <div class="site-logo mr-auto">
              <a href="index.html">Kira Kids<span class="text-primary">.</span></a>
            </div>
            <div class="site-quick-contact d-none d-lg-flex ml-auto ">
              <div class="d-flex site-info align-items-center mr-5">
                <span class="block-icon mr-3"><span class="icon-map-marker text-yellow"></span></span>
                <span>Avenida Salgado Filho, 666 - <br> São Paulo</span>
              </div>
              <div class="d-flex site-info align-items-center">
                <span class="block-icon mr-3"><span class="icon-clock-o"></span></span>
                <span> Dia 29 de setembro <br> </span>
              </div>
            </div>
          </div>
        </div>

        <div class="container">
          <div class="menu-wrap d-flex align-items-center">
            <span class="d-inline-block d-lg-none"><a href="#" class="text-black site-menu-toggle js-menu-toggle py-5"><span class="icon-menu h3 text-black"></span></a></span>

              <nav class="site-navigation text-left mr-auto d-none d-lg-block" role="navigation">
                <ul class="site-menu main-menu js-clone-nav mr-auto ">
                  <li><a href="<?= base_url('cliente/index') ?>" class="nav-link">Home</a></li>
                  <li><a href="<?= base_url('cliente/about') ?>" class="nav-link">Sobre</a></li>
                  <li><a href="<?= base_url('cliente/packages') ?>" class="nav-link">Pacotes</a></li>
                  <li><a href="<?= base_url('cliente/gallery') ?>" class="nav-link">Galeria</a></li>
                  <li><a href="<?= base_url('cliente/pricing') ?>" class="nav-link">Valores</a></li>
                  <li><a href="<?= base_url('cliente/contact') ?>" class="nav-link">Fale Conosco</a></li>
                  <li><a href="<?= base_url('admin/index') ?>" class="nav-link"> Cadastrar </a></li>
                </ul>
              </nav>

              <div class="top-social ml-auto">
                <a href="#"><span class="icon-facebook text-teal"></span></a>
                <a href="#"><span class="icon-twitter text-success"></span></a>
                <a href="#"><span class="icon-linkedin text-yellow"></span></a>
              </div>
          </div>
        </div>
      </header>
      
     
    