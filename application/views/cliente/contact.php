
    <div class="ftco-blocks-cover-1">
      <div class="site-section-cover overlay" data-stellar-background-ratio="0.5" style="background-image: url('../assets/images/hero_1.jpg')">
        <div class="container">
          <div class="row align-items-center ">

            <div class="col-md-5 mt-5 pt-5">
              <span class="text-cursive h5 text-red">Fale Conosco</span>
              <h1 class="mb-3 font-weight-bold text-teal"><?= $dados ?></h1>
              <p><a href="<?= base_url('cliente/index') ?>" class="text-white">Home</a> <span class="mx-3">/</span> <strong>Fale Conosco</strong></p>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="site-section bg-light" id="contact-section">
      <div class="container">
        <div class="row justify-content-center text-center">
        <div class="col-7 text-center mb-5">
          <h2>Entre em contato usando o formulário de contato</h2>
          <!--Validação do Formulário-->
          <?php
              if($FormError):
                  echo '<p>'.$FormError.'</p>';
              endif;
          ?>
        </div>
      </div>

        <div class="row">
          <div class="col-lg-8 mb-5" >
            <form action="#" method="post">
              <div class="form-group row">
                <div class="col-md-12 mb-4 mb-lg-0">
                  <label for="nome">Seu Nome</label>
                  <input type="text" class="form-control" placeholder="Nome" name="nome" id="nome">
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-6">
                 <label for="email">Seu Email</label>
                  <input type="text" class="form-control" placeholder="Email"  name="email" id="email" >
                </div>
    
                <div class="col-md-6">
                  <label for="assunto">Assunto</label>
                  <input type="text" class="form-control" placeholder="Assunto"  name="assunto" id="assunto" >
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-12">
                  <label for="mensagem">Sua Mensagem</label>
                  <textarea class="form-control" placeholder="Sua mensagem" name="mensagem" id="mensagem" cols="30" rows="10"></textarea>
                </div>
              </div>

              <div class="form-group row">
                <div class="col-md-6 mr-auto">
                  <input type="submit" class="btn btn-block btn-primary text-white py-3 px-5" value="Send Message">
                </div>
              </div>
            </form>
          </div>
          
          <div class="col-lg-4 ml-auto">
            <div class="bg-white p-3 p-md-5">
              <h3 class="text-black mb-4">Informações de Contato: </h3>
              <ul class="list-unstyled footer-link">
                <li class="d-block mb-3">
                  <span class="d-block text-black">Endereço:</span>
                  <span>Avenida Salgado Filho Name, Guarulhos ,São Paulo</span></li>
                <li class="d-block mb-3"><span class="d-block text-black">Telefone:</span><span>11 90000 0000</span></li>
                <li class="d-block mb-3"><span class="d-block text-black">Email:</span><span>kirakids@gmail.com</span></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>



    

    
